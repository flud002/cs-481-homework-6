﻿//Below are all of the imports that I used in this project
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Plugin.Connectivity;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Homework_6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    
    public partial class MainPage : ContentPage//Class that holds all of the contents of the mainpage
    {   
            public MainPage()//Calls the mainpage to be that page that appears on start
        {
            InitializeComponent();//Initializes all of the contents in the app
        }
        void DoIHaveInternet(Object sender, EventArgs e)//function that is tied to the connectivity button that shows whether the user is connected to the internet
        {
            Button button = sender as Button;//the button so you can manipulate components of the button
            if (CrossConnectivity.Current.IsConnected)//if the user is connected to the internet
            {
                button.Text = "Connected";//Changes the button text to connected
            }
            else//if the user is not connected to the internet
            {
                button.Text = "Not Connected";//Changes the button text to Not connected
            }

        }

        //function that gets the entry word from the user and process it with the Owl Bot Api to display the Type Definition and Example of the word
        async void Get_Word(Object sender,EventArgs e)
        {
            string entry = Entry.Text;
            HttpClient client = new HttpClient();//creates the client and initializes it to a new HttpClient

            var uri = new Uri(
                string.Format(
                    $"https://owlbot.info/api/v2/dictionary/" + entry));//Gets you to the page that is the same as the word that is entered

            var request = new HttpRequestMessage();//creates the request and initializes it to a new HttpRequestMessage
            request.Method = HttpMethod.Get;//initializes the request method to a new HttpMethod.Get
            request.RequestUri = uri;//initializes the request Uri to the uri we established above


            HttpResponseMessage response = await client.SendAsync(request);//send the client request
            
            Definition[] def = null;//initialises and array of Definitions

            if(response.IsSuccessStatusCode)//if the response comes back as succesful
            {
                var content = await response.Content.ReadAsStringAsync();//waiths for the content from the API and reads it as a string
                def = Definition.FromJson(content);//gets the content from the API
                if(def!=null)//if the array isnt empty
                {
                    typ.Text = $"The type of the word is: {def[0].Type}";//changes the typ label to the type of word from the web service 
                    Define.Text = $"The definition of the word is: {def[0].Def}";//changes the Define label to the definition of the word from the web service 
                    Examp.Text = $"An Example of the word is:  {def[0].Example}";//changes the Examp label to the example of the word from the web service 

                }
                
            }
        }
    }
}

