﻿//The namespace Quicktype that holds all of the JSON code that was converted to C#
namespace Homework_6
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;


    public partial class Definition
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definitions")]
        public string Def { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }
    }

    public partial class Definition
    {
        public static Definition[] FromJson(string json) => JsonConvert.DeserializeObject<Definition[]>(json, Homework_6.Converter.Settings);

    }

    public static class Serialize
    {
        public static string ToJson(this Definition self) => JsonConvert.SerializeObject(self, Homework_6.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}